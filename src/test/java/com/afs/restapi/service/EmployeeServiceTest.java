package com.afs.restapi.service;

import com.afs.restapi.exception.EmployeeAgeAndSalaryErrorException;
import com.afs.restapi.exception.EmployeeAgeErrorException;
import com.afs.restapi.exception.EmployeeDeletedNotUpdateException;
import com.afs.restapi.model.Employee;
import com.afs.restapi.repository.EmployeeRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class EmployeeServiceTest {

    private EmployeeRepository employeeRepository = mock(EmployeeRepository.class);

    private EmployeeService employeeService = new EmployeeService(employeeRepository);

    @Test
    void should_return_employee_age_error_when_add_employee_given_age_below_18_or_over_65() {
        // given
        Employee employeeYoung = new Employee(1, "lily1", 15, "female", 10100);
        Employee employeeOld = new Employee(2, "lily2", 88, "male", 20100);

        // when then
        Assertions.assertThrows(EmployeeAgeErrorException.class, () -> employeeService.insert(employeeYoung));
        Assertions.assertThrows(EmployeeAgeErrorException.class, () -> employeeService.insert(employeeOld));
        verify(employeeRepository, times(0)).insert(employeeYoung);
        verify(employeeRepository, times(0)).insert(employeeOld);

    }

    @Test
    void should_return_employee_salary_not_match_age_error_when_add_employee_given_employee_with_age_over_30_and_salary_below_20000() {
        // given
        Employee employee1 = new Employee(1, "lily1", 35, "female", 10000);

        // when then
        Assertions.assertThrows(EmployeeAgeAndSalaryErrorException.class, () -> employeeService.insert(employee1));

        verify(employeeRepository, times(0)).insert(employee1);
    }

    @Test
    void should_return_employee_with_status_true_when_add_employee_given_employee_match_rule() {
        // given
        Employee employee = new Employee(1, "lily1", 35, "female", 30000);
        Employee employeeToReturn = new Employee(1, "lily1", 35, "female", 30000);
        employeeToReturn.setStatus(true);
        when(employeeRepository.insert(any())).thenReturn(employeeToReturn);

        // when
        Employee employeeSaved = employeeService.insert(employee);

        // then
        assertTrue(employeeSaved.isStatus());
        verify(employeeRepository).insert(argThat(employeeToSave -> {
            assertTrue(employeeToSave.isStatus());
            return true;
        }));
    }

    @Test
    void should_return_employee_with_status_false_when_delete_employee_given_employee_match_rule() {
        // given
        Employee employeeToDelete = new Employee(1, "lily1", 35, "female", 30000);
        employeeToDelete.setStatus(true);
        when(employeeRepository.findById(1)).thenReturn(employeeToDelete);

        // when
        Employee deletedEmployee = employeeService.deleteEmployeeByupdateStatus(1);

        // then
        assertFalse(deletedEmployee.isStatus());
        verify(employeeRepository).findById(Mockito.eq(1));
    }

    @Test
    void should_return_employee_updated_when_update_employee_given_employee_with_status_false() {
        // given
        Employee employeeToUpdate = new Employee(1, "lily", 35, "female", 30000);
        employeeToUpdate.setStatus(false);
        when(employeeRepository.findById(1)).thenReturn(employeeToUpdate);
        Employee employeeHasUpdate = new Employee(1, "lily", 30, "female", 30000);
        employeeHasUpdate.setStatus(false);
        when(employeeRepository.update(1, employeeToUpdate)).thenReturn(employeeHasUpdate);

        // when then
        assertThrows(EmployeeDeletedNotUpdateException.class, () -> employeeService.update(1, employeeToUpdate));
        verify(employeeRepository, times(0)).update(1, employeeToUpdate);

    }

    @Test
    void should_return_employee_updated_when_update_employee_given_employee_with_status_true() {
        // given
        Employee employeeToUpdate = new Employee(1, "lily", 35, "female", 30000);
        employeeToUpdate.setStatus(true);
        when(employeeRepository.findById(1)).thenReturn(employeeToUpdate);
        Employee employeeHasUpdate = new Employee(1, "lily", 32, "female", 35000);
        when(employeeRepository.update(1, employeeToUpdate)).thenReturn(employeeHasUpdate);

        // when
        Employee employeeIsUpdated = employeeService.update(1, employeeToUpdate);

        // then
        assertEquals(employeeHasUpdate, employeeIsUpdated);
        verify(employeeRepository, times(1)).update(1, employeeToUpdate);

    }

}