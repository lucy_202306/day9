package com.afs.restapi;

import com.afs.restapi.exception.NotFoundCompanyException;
import com.afs.restapi.exception.NotFoundException;
import com.afs.restapi.model.Company;
import com.afs.restapi.model.Employee;
import com.afs.restapi.repository.CompanyRepository;
import com.afs.restapi.repository.EmployeeRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static org.hamcrest.Matchers.hasSize;
import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
class CompanyControllerTest {
    @Autowired
    CompanyRepository companyRepository;

    @Autowired
    MockMvc mockMvc;

    ObjectMapper mapper = new ObjectMapper();

    @BeforeEach
    void setUp() {
        companyRepository.clearAll();
    }

    private Company buildCompanyHUAWEI() {
        Company company = new Company(1, "HUAWEI", buildEmployeeList1());
        return company;
    }

    private Company buildCompanyApple() {
        return new Company(2, "APPLE", buildEmployeeList2());
    }

    private List<Employee> buildEmployeeList1() {
        List<Employee> employeeList = new ArrayList<>() {{
            add(new Employee(1, "Susan", 22, "Female", 10000));
        }};
        return employeeList;
    }

    private List<Employee> buildEmployeeList2() {
        List<Employee> employeeList = new ArrayList<>() {{
            add(new Employee(1, "Lily", 22, "Female", 10000));
        }};
        return employeeList;
    }

    @Test
    void should_get_all_companies_when_perform_get_given_companies_use_matchers() throws Exception {
        //given
        Company companyHUAWEI = buildCompanyHUAWEI();
        Company companyAPPLE = buildCompanyApple();
        companyRepository.addCompany(companyHUAWEI);
        companyRepository.addCompany(companyAPPLE);

        //when
        mockMvc.perform(MockMvcRequestBuilders.get("/companies"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$[0].id").isNumber())
                .andExpect(jsonPath("$[0].id").value(1))
                .andExpect(jsonPath("$[0].companyName").isString())
                .andExpect(jsonPath("$[0].companyName").value("HUAWEI"))
                .andExpect(jsonPath("$[0].employees[0].id").isNumber())
                .andExpect(jsonPath("$[0].employees[0].id").value(1))
                .andExpect(jsonPath("$[0].employees[0].name").isString())
                .andExpect(jsonPath("$[0].employees[0].name").value("Susan"))
                .andExpect(jsonPath("$[0].employees[0].age").isNumber())
                .andExpect(jsonPath("$[0].employees[0].age").value(22))
                .andExpect(jsonPath("$[0].employees[0].gender").isString())
                .andExpect(jsonPath("$[0].employees[0].gender").value("Female"))
                .andExpect(jsonPath("$[0].employees[0].salary").isNumber())
                .andExpect(jsonPath("$[0].employees[0].salary").value(10000))
                .andExpect(jsonPath("$[1].id").isNumber())
                .andExpect(jsonPath("$[1].id").value(2))
                .andExpect(jsonPath("$[1].companyName").isString())
                .andExpect(jsonPath("$[1].companyName").value("APPLE"))
                .andExpect(jsonPath("$[1].employees[0].id").isNumber())
                .andExpect(jsonPath("$[1].employees[0].id").value(1))
                .andExpect(jsonPath("$[1].employees[0].name").isString())
                .andExpect(jsonPath("$[1].employees[0].name").value("Lily"))
                .andExpect(jsonPath("$[1].employees[0].age").isNumber())
                .andExpect(jsonPath("$[1].employees[0].age").value(22))
                .andExpect(jsonPath("$[1].employees[0].gender").isString())
                .andExpect(jsonPath("$[1].employees[0].gender").value("Female"))
                .andExpect(jsonPath("$[1].employees[0].salary").isNumber())
                .andExpect(jsonPath("$[1].employees[0].salary").value(10000));
    }

    @Test
    void should_return_company_HUAWEI_with_id_1_when_perform_get_by_id_given_companies_in_repo() throws Exception {
        //given
        Company companyHUAWEI = buildCompanyHUAWEI();
        Company companyAPPLE = buildCompanyApple();
        companyRepository.addCompany(companyHUAWEI);
        companyRepository.addCompany(companyAPPLE);

        //when
        mockMvc.perform(MockMvcRequestBuilders.get("/companies/1"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(jsonPath("$.id").isNumber())
                .andExpect(jsonPath("$.id").value(1))
                .andExpect(jsonPath("$.companyName").isString())
                .andExpect(jsonPath("$.companyName").value("HUAWEI"))
                .andExpect(jsonPath("$.employees[0].id").isNumber())
                .andExpect(jsonPath("$.employees[0].id").value(1))
                .andExpect(jsonPath("$.employees[0].name").isString())
                .andExpect(jsonPath("$.employees[0].name").value("Susan"))
                .andExpect(jsonPath("$.employees[0].age").isNumber())
                .andExpect(jsonPath("$.employees[0].age").value(22))
                .andExpect(jsonPath("$.employees[0].gender").isString())
                .andExpect(jsonPath("$.employees[0].gender").value("Female"))
                .andExpect(jsonPath("$.employees[0].salary").isNumber())
                .andExpect(jsonPath("$.employees[0].salary").value(10000));
    }

    @Test
    void should_return_employees_by_company_id_with_1_when_perform_get_by_id_given_company_id_in_repo() throws Exception {
        //given
        Company companyHUAWEI = buildCompanyHUAWEI();
        Company companyAPPLE = buildCompanyApple();
        companyRepository.addCompany(companyHUAWEI);
        companyRepository.addCompany(companyAPPLE);

        //when
        mockMvc.perform(MockMvcRequestBuilders.get("/companies/{companyId}/employees", 1))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(jsonPath("$[0].id").isNumber())
                .andExpect(jsonPath("$[0].id").value(1))
                .andExpect(jsonPath("$[0].name").isString())
                .andExpect(jsonPath("$[0].name").value("Susan"))
                .andExpect(jsonPath("$[0].age").isNumber())
                .andExpect(jsonPath("$[0].age").value(22))
                .andExpect(jsonPath("$[0].gender").isString())
                .andExpect(jsonPath("$[0].gender").value("Female"))
                .andExpect(jsonPath("$[0].salary").isNumber())
                .andExpect(jsonPath("$[0].salary").value(10000));
    }

    @Test
    void should_return_employees_by_page_and_sizewhen_perform_get_given_companies_and_page_size_in_repo() throws Exception {
        //given
        Company companyHUAWEI = buildCompanyHUAWEI();
        Company companyAPPLE = buildCompanyApple();
        companyRepository.addCompany(companyHUAWEI);
        companyRepository.addCompany(companyAPPLE);

        //when
        mockMvc.perform(MockMvcRequestBuilders.get("/companies?pageIndex={pageIndex}&pageSize={pageSize}", 1, 1))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(jsonPath("$[0].id").isNumber())
                .andExpect(jsonPath("$[0].id").value(1))
                .andExpect(jsonPath("$[0].companyName").isString())
                .andExpect(jsonPath("$[0].companyName").value("HUAWEI"))
                .andExpect(jsonPath("$[0].employees[0].id").isNumber())
                .andExpect(jsonPath("$[0].employees[0].id").value(1))
                .andExpect(jsonPath("$[0].employees[0].name").isString())
                .andExpect(jsonPath("$[0].employees[0].name").value("Susan"))
                .andExpect(jsonPath("$[0].employees[0].age").isNumber())
                .andExpect(jsonPath("$[0].employees[0].age").value(22))
                .andExpect(jsonPath("$[0].employees[0].gender").isString())
                .andExpect(jsonPath("$[0].employees[0].gender").value("Female"))
                .andExpect(jsonPath("$[0].employees[0].salary").isNumber())
                .andExpect(jsonPath("$[0].employees[0].salary").value(10000));
    }

    @Test
    void should_return_company_save_with_id_when_perform_post_given_a_company() throws Exception {
        //given
        Company company = buildCompanyHUAWEI();
        String companyJson = mapper.writeValueAsString(company);

        //when
        mockMvc.perform(MockMvcRequestBuilders.post("/companies")
                .contentType(MediaType.APPLICATION_JSON)
                .content(companyJson));
        Company companySaved = companyRepository.findCompanyById(1);
        assertEquals(company.getCompanyName(), companySaved.getCompanyName());
        assertEquals(company.getId(), companySaved.getId());
    }

    @Test
    void should_update_ecompany_in_repo_when_perform_put_by_id_given_company_in_repo_and_update_info() throws Exception {

        //given
        Company companyHUAWEI = buildCompanyHUAWEI();
        companyRepository.addCompany(companyHUAWEI);
        Company companyUpdated = new Company(1, "APPLE", buildEmployeeList1());
        String companyUpdatedJson = mapper.writeValueAsString(companyUpdated);

        //when
        mockMvc.perform(MockMvcRequestBuilders.put("/companies/{companyId}", 1)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(companyUpdatedJson))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").isNumber())
                .andExpect(jsonPath("$.id").value(1))
                .andExpect(jsonPath("$.companyName").isString())
                .andExpect(jsonPath("$.companyName").value("APPLE"))
                .andExpect(jsonPath("$.employees[0].id").isNumber())
                .andExpect(jsonPath("$.employees[0].id").value(1))
                .andExpect(jsonPath("$.employees[0].name").isString())
                .andExpect(jsonPath("$.employees[0].name").value("Susan"))
                .andExpect(jsonPath("$.employees[0].age").isNumber())
                .andExpect(jsonPath("$.employees[0].age").value(22))
                .andExpect(jsonPath("$.employees[0].gender").isString())
                .andExpect(jsonPath("$.employees[0].gender").value("Female"))
                .andExpect(jsonPath("$.employees[0].salary").isNumber())
                .andExpect(jsonPath("$.employees[0].salary").value(10000));
    }

    @Test
    void should_del_company_in_repo_when_perform_del_by_id_given_companies() throws Exception {
        //given
        Company company = buildCompanyHUAWEI();
        companyRepository.addCompany(company);

        //when
        mockMvc.perform(MockMvcRequestBuilders.delete("/companies/{companyId}", 1))
                .andExpect(status().isNoContent());

        //then
        assertThrows(NotFoundCompanyException.class, () -> companyRepository.findCompanyById(1));
    }

}