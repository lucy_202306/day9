package com.afs.restapi.controller;


import com.afs.restapi.model.Company;
import com.afs.restapi.model.Employee;
import com.afs.restapi.repository.CompanyRepository;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;


@RestController
@RequestMapping("/companies")
public class CompanyController {
    private final CompanyRepository companyRepository;

    public CompanyController(CompanyRepository companyRepository) {
        this.companyRepository = companyRepository;
    }

    @GetMapping
    public List<Company> getCompanies() {
        return companyRepository.getCompanies();
    }

    @GetMapping("/{companyId}")
    public Company getCompanyById(@PathVariable Integer companyId) {
        return companyRepository.getCompanies().stream()
                .filter(company -> company.getId().equals(companyId))
                .findFirst()
                .orElse(null);
    }

    @GetMapping("/{companyId}/employees")
    public List<Employee> getEmployeesByCompanyId(@PathVariable Integer companyId) {
        return companyRepository.getCompanies().stream()
                .filter(company -> company.getId().equals(companyId))
                .findFirst()
                .map(company -> company.getEmployees())
                .orElse(Collections.emptyList());
    }

    @GetMapping(params = {"pageIndex", "pageSize"})
    public List<Company> getCompaniesByPagination(@RequestParam Integer pageIndex,
                                                  @RequestParam Integer pageSize) {
        return companyRepository.getCompanies().stream()
                .skip((pageIndex - 1) * pageSize)
                .limit(pageSize)
                .collect(Collectors.toList());
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public void addCompany(@RequestBody Company company) {
        companyRepository.addCompany(company);
    }

    @PutMapping("/{companyId}")
    public Company updateCompany(@PathVariable Integer companyId, @RequestBody Company company) {
        return companyRepository.getCompanies().stream()
                .filter(storedCompany -> storedCompany.getId().equals(companyId))
                .findFirst()
                .map(storedCompany -> updateCompanyAttributes(storedCompany, company))
                .orElse(null);
    }

    private Company updateCompanyAttributes(Company companyStored, Company company) {
        if (company.getCompanyName() != null) {
            companyStored.setCompanyName(company.getCompanyName());
        }
        return companyStored;
    }

    @DeleteMapping("/{companyId}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void remove(@PathVariable Integer companyId) {
        companyRepository.getCompanies().removeIf(company -> company.getId().equals(companyId));
    }
}
