package com.afs.restapi.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class NotFoundCompanyException extends RuntimeException {
    public NotFoundCompanyException() {
        super("company id not found");
    }
}

