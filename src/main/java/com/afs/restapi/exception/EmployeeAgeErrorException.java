package com.afs.restapi.exception;

public class EmployeeAgeErrorException extends RuntimeException {
    public EmployeeAgeErrorException() {
        super("employee age error");
    }
}
