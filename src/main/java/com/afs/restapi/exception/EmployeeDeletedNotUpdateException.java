package com.afs.restapi.exception;

public class EmployeeDeletedNotUpdateException extends RuntimeException {
    public EmployeeDeletedNotUpdateException() {
        super("employee has been deleted and not update");
    }
}
