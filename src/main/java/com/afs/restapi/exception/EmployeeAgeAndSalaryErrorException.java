package com.afs.restapi.exception;

public class EmployeeAgeAndSalaryErrorException extends RuntimeException {
    public EmployeeAgeAndSalaryErrorException() {
        super("employee age over 30 and salary below 20000");
    }
}
