package com.afs.restapi.service;

import com.afs.restapi.exception.EmployeeAgeAndSalaryErrorException;
import com.afs.restapi.exception.EmployeeAgeErrorException;
import com.afs.restapi.exception.EmployeeDeletedNotUpdateException;
import com.afs.restapi.model.Employee;
import com.afs.restapi.repository.EmployeeRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EmployeeService {

    private EmployeeRepository employeeRepository;

    public EmployeeService(EmployeeRepository employeeRepository) {
        this.employeeRepository = employeeRepository;
    }

    public List<Employee> getAll() {
        return employeeRepository.findAll();
    }

    public Employee findById(int id) {
        return employeeRepository.findById(id);
    }

    public List<Employee> findByGender(String gender) {
        return employeeRepository.findByGender(gender);
    }

    public List<Employee> findByPage(int pageNumber, int pageSize) {
        return employeeRepository.findByPage(pageNumber, pageSize);
    }

    public Employee insert(Employee employee) {
        if (employee.getAge() < 18 || employee.getAge() > 65) {
            throw new EmployeeAgeErrorException();
        }
        if (employee.getAge() > 30 && employee.getSalary() < 20000) {
            throw new EmployeeAgeAndSalaryErrorException();
        }
        employee.setStatus(true);
        return employeeRepository.insert(employee);
    }

    public Employee update(int id, Employee employee) {
        Employee employeeToUpdate = employeeRepository.findById(id);
        if(employeeToUpdate == null || !employeeToUpdate.isStatus()) {
            throw new EmployeeDeletedNotUpdateException();
        }
        return employeeRepository.update(id, employee);
    }

    public void delete(int id) {
        employeeRepository.delete(id);
    }

    public Employee deleteEmployeeByupdateStatus(int id) {
        Employee employee = employeeRepository.findById(id);
        if(employee != null) {
            employee.setStatus(false);
        }
        return employee;
    }

}